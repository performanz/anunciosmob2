//
//  PostAdsViewController.h
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/23/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostAdsViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webview;
@end
