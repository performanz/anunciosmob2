//
//  AccountsViewController.m
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/23/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import "AccountsViewController.h"
#import "WebViewController.h"

@interface AccountsViewController ()

@end

@implementation AccountsViewController



- (id)initWithCoder:(NSCoder *)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"Account";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"account_username";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = NO;
        
        // The number of objects to show per page
        //self.objectsPerPage = 10;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTable:)
                                                 name:@"refreshTable"
                                               object:nil];
    
    PFUser *user = [PFUser currentUser];
    if (user) {
        NSLog(@"Current user: %@", user.username);
    }
    else {
        [self performSegueWithIdentifier:@"showLogin" sender:self];
    }
//
//    // Find all posts by the current user
//    PFQuery *query = [PFQuery queryWithClassName:@"Account"];
//    [query orderByAscending:@"account_username"];
//    [query whereKey:@"user" equalTo:user];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (error) {
//            NSLog(@"Error %@ %@", error, [error userInfo]);
//        }
//        else {
//            self.accounts = objects;
//            [self.tableView reloadData];
//        }
//    }];
    
    }

- (void)refreshTable:(NSNotification *) notification
{
    // Reload the data
    [self loadObjects];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshTable" object:nil];
}

- (PFQuery *)queryForTable
{
    
    PFUser *user = [PFUser currentUser];
    
    // If the user is not logged in we simply return nil
    if (!user) {
        return nil;
    }
    
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    [query orderByAscending:@"account_username"];
    [query whereKey:@"user" equalTo:user];
    
    return query;
}


- (IBAction)logout:(id)sender {
    [PFUser logOut];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
    [self performSegueWithIdentifier:@"showLogin" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    

    
    if ([segue.identifier isEqualToString:@"showLogin"]) {
        [segue.destinationViewController setHidesBottomBarWhenPushed:YES];
    } else if ([segue.identifier isEqualToString:@"showAccountLogin"]){
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        PFObject *object = [self.objects objectAtIndex:indexPath.row];
        
        WebViewController *destViewController = segue.destinationViewController;
        destViewController.username = [object objectForKey:@"account_username"];
        destViewController.password = [object objectForKey:@"account_password"];
        destViewController.objectId = object.objectId;
    }
    
}

#pragma mark - Table view data source
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    PFObject *account = [self.accounts objectAtIndex:indexPath.row];
//    cell.textLabel.text = [account objectForKey:@"account_username"];
//    
//    return cell;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell to show todo item with a priority at the bottom
    cell.textLabel.text = [object objectForKey:@"account_username"];
    
    return cell;
}

@end
