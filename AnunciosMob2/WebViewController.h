//
//  ViewController.h
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/15/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface WebViewController : UIViewController <UIWebViewDelegate> {
    
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *passwordField;
    IBOutlet UITextField *captchaField;
    IBOutlet UIButton *loginBtn;
    IBOutlet UIButton *publishBtn;
    IBOutlet UIButton *deleteBtn;
}

@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) MBProgressHUD *hud;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *objectId;

@property (strong, nonatomic) IBOutlet UIWebView *webview1;
@property (strong, nonatomic) IBOutlet UIWebView *webview2;
@property (strong, nonatomic) IBOutlet UIWebView *webview3;
@property (strong, nonatomic) IBOutlet UIWebView *webview4;
@property (strong, nonatomic) IBOutlet UIWebView *webview5;

- (IBAction)login:(id)sender;
- (IBAction)deleteAccount:(id)sender;
- (IBAction)publishAds:(id)sender;

@end
