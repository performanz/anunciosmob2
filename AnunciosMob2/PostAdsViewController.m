//
//  PostAdsViewController.m
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/23/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import "PostAdsViewController.h"

@interface PostAdsViewController ()

@end

@implementation PostAdsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}



#pragma mark - Optional UIWebViewDelegate delegate methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    NSString* url= [webView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    NSLog(@"%@", url);
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
@end
