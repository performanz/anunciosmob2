//
//  ViewController.m
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/15/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import "WebViewController.h"
#import <Parse/Parse.h>
#import "PostAdsViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

@synthesize webview = _webView;

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Show progress
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    
    usernameField.text = self.username;
    passwordField.text = self.password;
    passwordField.hidden = YES;
    publishBtn.hidden = YES;
    NSString *urlString = @"http://www.telexfree.com.br";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.webview.delegate = self;
    
    [self.webview loadRequest:request ];
    [self.webview1 setHidden:YES ];
    [self.webview2 setHidden:YES ];
    [self.webview3 setHidden:YES ];
    [self.webview4 setHidden:YES ];
    [self.webview5 setHidden:YES ];
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showPostAds"]) {
        PostAdsViewController *destViewController = segue.destinationViewController;
        destViewController.webview = self.webview;
    }
    
}


- (IBAction)login:(id)sender {

//    f.find("input[name='log_usuario_sis']").val( $scope.account.username );
//    f.find("input[name='pwd_usuario_sis']").val( $scope.account.password );
//    console.log("User: "+  $scope.account.username );
//    console.log("Pwd:  "+  $scope.account.password );
//    f.find('#security_code').val( $('#security_code').val() );
//    f.find('#form_login').submit();
    
    [captchaField resignFirstResponder];
    //self.webview.hidden = YES;
    
    NSString *user  =    [NSString stringWithFormat: @"$(\"input[name='log_usuario_sis']\").val( \'%@\' );", usernameField.text];
    NSString *pass  =    [NSString stringWithFormat: @"$(\"input[name='pwd_usuario_sis']\").val( \'%@\' );", passwordField.text];
    NSString *code  =    [NSString stringWithFormat: @"$('#security_code').val( \'%@\' );", captchaField.text];
    
    NSString *js = [NSString stringWithFormat: @"%@ %@ %@", user, pass, code];

    [self.webview stringByEvaluatingJavaScriptFromString:js];
    [self.webview stringByEvaluatingJavaScriptFromString:@"$('#form_login').submit();"];
    
    //[self performSegueWithIdentifier:@"showPostAds" sender:self];
    
}


- (IBAction)deleteAccount:(id)sender {
    
    // Notify table view to reload the DATA from Parse cloud
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Wait" message:@"Are you sure you want to delete this.  This action cannot be undone" delegate:self cancelButtonTitle:@"Delete" otherButtonTitles:@"Cancel", nil];
    [alert show];
    

    

}

- (IBAction)publishAds:(id)sender {
    
    [self.hud show:YES];
    NSString *urlString = @"http://www.telexfree.com/bo/ad/setup3/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webview stringByEvaluatingJavaScriptFromString:@"window.location.href = 'http://www.telexfree.com/bo/ad/setup3/'"];
    [self.webview1 loadRequest:request ];
    [self.webview2 loadRequest:request ];
    [self.webview3 loadRequest:request ];
    [self.webview4 loadRequest:request ];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        
        PFObject *object = [PFObject objectWithoutDataWithClassName:@"Account"
                                                           objectId:self.objectId];
        [object deleteInBackground];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
        sleep(2);
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
}

#pragma mark - Optional UIWebViewDelegate delegate methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
    [self.hud show:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.webview stringByEvaluatingJavaScriptFromString:@"$('#divBanner').remove(); $('#divConteudo').remove(); $('#divRodape').remove(); $('#divLogo').remove(); $('#divMenu').remove(); $('#divIndicacao').remove(); $('#divIdioma').remove(); $('#divTopo').hide(); $('#siimage-login').width('100%'); $('#siimage-login').height('100%'); $('body').append($('#siimage-login'));"];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    NSString* url= [webView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    NSLog(@"%@", url);
    
    if ([url isEqualToString:@"http://www.telexfree.com/bo/"] || [url isEqualToString:@"http://www.telexfree.com/bo/home"] ) {

        self.webview.hidden = YES;
        loginBtn.hidden = YES;
        deleteBtn.hidden = YES;
        publishBtn.hidden = NO;

        
    }else if ([url isEqualToString:@"http://www.telexfree.com/bo/ad/setup3/"]){
        
        NSString *postAds = @"function postAD(id){ $(\"input[name='location']\").val(result); $(\"input[name='url']\").val(result); $('[name=adcentral] option').filter(function() { return ( $(this).val() == id ); }).prop('selected', true); $(\"form[name=setup4]\").submit(); }";
        
        NSString *username = [self.username lowercaseString];;

        NSString *adcount= [webView stringByEvaluatingJavaScriptFromString:@"$('[name=adcentral] option').length;"];
        
        NSLog(@"ADCOUNT: %@", adcount);
        if( [adcount isEqualToString:@"1"] ){
            
            long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            NSString *result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(0);"  ]];
            
        } else if ([adcount isEqualToString:@"2"]){
            
            long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            NSString *result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            
            [self.webview1 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(1);"  ]];
            sleep(0.5);
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(0);"  ]];
            
        }else if ([adcount isEqualToString:@"3"]){
            
            long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            NSString *result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview1 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(1);"  ]];
            sleep(0.5);
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview2 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(2);"  ]];
            sleep(0.5);
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(0);"  ]];

        }else if ([adcount isEqualToString:@"4"]){
            
            long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            NSString *result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview1 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(1);"  ]];
            sleep(0.5);
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview2 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(2);"  ]];
            sleep(0.5);
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview3 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(3);"  ]];
            sleep(0.5);
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(0);"  ]];

        }else if ([adcount isEqualToString:@"5"]){
            
            long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970] +1);
            NSString *result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview1 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(1);"  ]];
            sleep(0.5);
            NSLog(@"Current Time: %ld",currentTime+1);
            
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970] +2);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview2 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(2);"  ]];
            sleep(0.5);
            NSLog(@"Current Time: %ld",currentTime+2);
            
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970] +3);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview3 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(3);"  ]];
            sleep(0.5);
            NSLog(@"Current Time: %ld",currentTime+3);
            
            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970] +4);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview4 stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(4);"  ]];
            sleep(0.5);
            NSLog(@"Current Time: %ld",currentTime+4);

            currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970] +5);
            result   = [NSString stringWithFormat:@"var result = 'http://adsfree.aws.af.cm/ads/%@/%ld'; ", username,currentTime];
            [self.webview stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@ %@ %@ ", result, postAds, @"postAD(0);"  ]];
            NSLog(@"Current Time: %ld",currentTime+5);
        }
        
        
        
        [self.webview5 stringByEvaluatingJavaScriptFromString: @"window.location.href = 'http://www.telexfree.com/bo/adcentralad/'"];
        
        
        //[self.webview5 setHidden:NO ];
        
    } else if([url isEqualToString:@"http://www.telexfree.com/bo/popup/"]) {
        self.webview.hidden = YES;
        [self.webview stringByEvaluatingJavaScriptFromString:@"window.location.href = 'http://www.telexfree.com/bo/popup/confirm/45'"];
    } else if([url isEqualToString:@"http://www.telexfree.com"]) {
        self.webview.hidden = NO;
    } else if([url isEqualToString:@"http://www.telexfree.com/bo/adcentralad/"]) {
        [self performSelector:@selector(showResults) withObject:nil afterDelay:0.5];
    }
    
    
    [self.hud hide:YES];
}

- (void) showResults
{
    NSString *out =  [self.webview5 stringByEvaluatingJavaScriptFromString: @"$('.content').html()"];
    //NSString *out =  [self.webview5 stringByEvaluatingJavaScriptFromString: @"$('#tablee tr:eq(1) > td:eq(1)').text()"];
    
    NSLog(@"DATE: %@",out);

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
