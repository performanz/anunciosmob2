//
//  AddAccountViewController.h
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/23/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAccountViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)addAccount:(id)sender;
@end
