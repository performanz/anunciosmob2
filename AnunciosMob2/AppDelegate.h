//
//  AppDelegate.h
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/15/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
