//
//  AddAccountViewController.m
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/23/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import "AddAccountViewController.h"
#import <Parse/Parse.h>

@interface AddAccountViewController ()

@end

@implementation AddAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addAccount:(id)sender {
    
    PFUser *user = [PFUser currentUser];
    
    // Make a new post
    PFObject *acc = [PFObject objectWithClassName:@"Account"];
    [acc setObject:self.usernameField.text forKey:@"account_username"];
    [acc setObject:self.passwordField.text forKey:@"account_password"];
    [acc setObject:user forKey:@"user"];
    [acc saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                                message:[error.userInfo objectForKey:@"error"]
                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }else{
            // Notify table view to reload the DATA from Parse cloud
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:self];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
    
}
@end
