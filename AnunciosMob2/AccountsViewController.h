//
//  AccountsViewController.h
//  AnunciosMob2
//
//  Created by Diogenes Buarque on 9/23/13.
//  Copyright (c) 2013 Performanz Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface AccountsViewController : PFQueryTableViewController

- (IBAction)logout:(id)sender;

@end
